import styles from './ActivityIndicator.module.css';

export default function ActivityIndicator() {
  return (
    <div className={styles.center}>
      <div className={styles.loader}>
        <div />
      </div>
    </div>
  );
}
