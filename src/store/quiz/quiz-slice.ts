import { createSlice } from '@reduxjs/toolkit';

export type QuizData = {
  category: string | undefined;
  type: string | undefined;
  difficulty: string | undefined;
  question: string | undefined;
  correct_answer: boolean | string | undefined;
  incorrect_answers: boolean[];
};

type QuizStatus = 'notStarted' | 'started' | 'completed';

const initialState: {
  quizDataLoading: boolean;
  quizData: QuizData[];
  quizStatus: QuizStatus;
  userAnswer: boolean[];
  error: string | undefined;
} = {
  quizDataLoading: true,
  quizData: [
    {
      category: undefined,
      type: undefined,
      difficulty: undefined,
      question: undefined,
      correct_answer: undefined,
      incorrect_answers: [],
    },
  ],
  quizStatus: 'notStarted',
  userAnswer: [],
  error: undefined,
};

export const quizSlice = createSlice({
  name: 'quiz',
  initialState,
  reducers: {
    quizDataLoading(state, action: { payload: boolean }) {
      state.quizDataLoading = action.payload;
    },
    setQuizData(state, action: { payload: QuizData[] }) {
      state.quizData = action.payload;
    },
    setError(state, action) {
      state.error = action.payload;
    },
    quizStatus(state, action: { payload: QuizStatus }) {
      state.quizStatus = action.payload;
    },
    postAnswer(state, action: { payload: boolean }) {
      state.userAnswer.push(action.payload);
    },
    initState: () => initialState,
    errorConfirmation(state) {
      state.error = undefined;
    },
  },
});

export const quizActions = quizSlice.actions;
