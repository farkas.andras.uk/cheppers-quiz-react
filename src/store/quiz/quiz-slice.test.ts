import { quizSlice, quizActions } from './quiz-slice';

test('should return the initial state', () => {
  expect(quizSlice.reducer(undefined, quizActions.initState)).toEqual({
    error: undefined,
    quizData: [
      {
        category: undefined,
        correct_answer: undefined,
        difficulty: undefined,
        incorrect_answers: [],
        question: undefined,
        type: undefined,
      },
    ],
    quizDataLoading: true,
    quizStatus: 'notStarted',
    userAnswer: [],
  });
});

test('should handle a loading state change', () => {
  expect(
    quizSlice.reducer(undefined, quizActions.quizDataLoading(false))
  ).toEqual({
    error: undefined,
    quizData: [
      {
        category: undefined,
        correct_answer: undefined,
        difficulty: undefined,
        incorrect_answers: [],
        question: undefined,
        type: undefined,
      },
    ],
    quizDataLoading: false,
    quizStatus: 'notStarted',
    userAnswer: [],
  });
});
