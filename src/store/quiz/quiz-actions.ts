import { AppDispatch } from '..';
import { quizActions } from './quiz-slice';

import { QuizData } from './quiz-slice';

export const getQuizData = () => {
  return async (dispatch: AppDispatch) => {
    dispatch(quizActions.quizDataLoading(true));
    try {
      const responseData = await fetch(
        'https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean'
      );
      const jsonData = await responseData.json();
      const correctedQuizData = jsonData.results.map((quizData: QuizData) => {
        return {
          ...quizData,
          correct_answer:
            typeof quizData.correct_answer === 'string'
              ? quizData.correct_answer.toLowerCase() === 'true'
                ? true
                : false
              : false,
        };
      });

      dispatch(quizActions.setQuizData(correctedQuizData));
      dispatch(quizActions.quizDataLoading(false));
    } catch (err) {
      dispatch(quizActions.quizDataLoading(false));
      dispatch(quizActions.setError(err));
    }
  };
};
