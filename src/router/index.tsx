import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';

import ActivityIndicator from '../components/ActivityIndicator/ActivityIndicator';
import HomeScreen from '../screens/HomeScreen';
import QuizScreen from '../screens/Quiz/QuizScreen';
import ResultsScreen from '../screens/Quiz/ResultsScreen';
import NotFound from '../components/NotFound';

import { getQuizData } from '../store/quiz/quiz-actions';

export default function Router() {
  const quiz = useSelector((state: RootState) => state.quiz);
  const quizStatus = useSelector((state: RootState) => state.quiz.quizStatus);
  const quizData = useSelector((state: RootState) => state.quiz.quizData);
  const userAnswer = useSelector((state: RootState) => state.quiz.userAnswer);

  console.log({
    quizData,
    userAnswer,
    quizStatus,
  });

  const dispatch = useDispatch();
  useEffect(() => {
    if (quizStatus === 'notStarted') {
      dispatch(getQuizData());
    }
  }, [dispatch, quizStatus]);

  const Quiz = useCallback(() => {
    return quizStatus === 'started' ? (
      <QuizScreen />
    ) : quizStatus === 'completed' ? (
      <Navigate to={'results'} />
    ) : (
      <Navigate to={'/home'} />
    );
  }, [quizStatus]);

  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Navigate to={'home'} />} />
        <Route
          path='home'
          element={
            quiz.quizDataLoading ? <ActivityIndicator /> : <HomeScreen />
          }
        />
        <Route path='quiz' element={<Quiz />} />
        <Route
          path='quiz/results'
          element={
            quizStatus === 'notStarted' ? (
              <Navigate to={'/home'} />
            ) : (
              <ResultsScreen />
            )
          }
        />
        <Route path='*' element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}
