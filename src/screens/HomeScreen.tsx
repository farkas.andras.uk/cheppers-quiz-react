import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { quizActions } from '../store/quiz/quiz-slice';

import styles from './HomeScreen.module.css';

export default function HomeScreen() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const startQuiz = () => {
    dispatch(quizActions.quizStatus('started'));
    navigate('/quiz');
  };

  return (
    <div className={styles.container}>
      <h1>Welcome to the Trivia Challenge!</h1>
      <p>You will be presented with 10 True or False questions.</p>
      <p>Can you scrore 100%?</p>
      <button className={styles.button} onClick={startQuiz}>
        BEGIN
      </button>
    </div>
  );
}
