import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store';

import { quizActions } from '../../store/quiz/quiz-slice';

import styles from './ResultsScreen.module.css';

export default function ResultsScreen() {
  const dispatch = useDispatch();
  const quizData = useSelector((state: RootState) => state.quiz.quizData);
  const userAnswer = useSelector((state: RootState) => state.quiz.userAnswer);

  let scoreCount = 0;
  const ScoreList = quizData.map((quiz, index) => {
    if (quiz.correct_answer === userAnswer[index]) {
      scoreCount += 1;
      return (
        <p
          key={index}
          className={`${styles.mediumFontSize} ${styles.correctAnswer}`}
        >{`+ ${quiz.question}`}</p>
      );
    } else {
      return (
        <p
          key={index}
          className={`${styles.mediumFontSize} ${styles.incorrectAnswer}`}
        >{`- ${quiz.question}`}</p>
      );
    }
  });

  return (
    <div className={`${styles.container}`}>
      <div className={styles.resultContainer}>
        <h1>{`Your scored ${scoreCount} / ${quizData.length}`}</h1>
      </div>
      <div>{ScoreList}</div>
      <button
        className={styles.button}
        onClick={() => {
          dispatch(quizActions.initState());
        }}
      >
        PLAY AGAIN?
      </button>
    </div>
  );
}
