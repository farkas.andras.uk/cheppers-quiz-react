import { useDispatch } from 'react-redux';
import { quizActions } from '../../store/quiz/quiz-slice';

import styles from './Question.module.css';

type QuestionProps = {
  quizQuestion: {
    category: string | undefined;
    question: string | undefined;
    questionNum: number;
    questionNumOf: number;
  };
};

export default function Question(props: QuestionProps) {
  const { quizQuestion } = props;
  const dispatch = useDispatch();

  return (
    <div style={{ textAlign: 'center' }}>
      <h1>{quizQuestion.category}</h1>
      <div className={styles.questionBox}>
        <p className={styles.questionText}>{quizQuestion.question}</p>
      </div>
      <p>{`${quizQuestion.questionNum} of ${quizQuestion.questionNumOf}`}</p>
      <div className={styles.buttonContainer}>
        <button
          className={styles.button}
          onClick={() => dispatch(quizActions.postAnswer(true))}
        >
          TRUE
        </button>
        <button
          className={styles.button}
          onClick={() => dispatch(quizActions.postAnswer(false))}
        >
          FALSE
        </button>
      </div>
    </div>
  );
}
