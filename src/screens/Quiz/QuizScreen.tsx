import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store';

import Question from './Question';

import { quizActions } from '../../store/quiz/quiz-slice';

import style from './QuizScreen.module.css';

export default function QuizScreen() {
  const dispatch = useDispatch();
  const quizData = useSelector((state: RootState) => state.quiz.quizData);
  const userAnswer = useSelector((state: RootState) => state.quiz.userAnswer);
  const quizQuestion = quizData[userAnswer.length];

  useEffect(() => {
    if (quizData.length === userAnswer.length) {
      dispatch(quizActions.quizStatus('completed'));
    }
  }, [dispatch, quizData, userAnswer]);

  return (
    <div className={style.container}>
      {quizData.length !== userAnswer.length ? (
        <Question
          quizQuestion={{
            category: quizQuestion.category,
            question: quizQuestion.question,
            questionNum: userAnswer.length,
            questionNumOf: quizData.length,
          }}
        />
      ) : null}
    </div>
  );
}
